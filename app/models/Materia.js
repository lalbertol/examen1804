const mongoose = require('mongoose')
const Schema = mongoose.Schema

const materiaSchema = new Schema({
  nombre: {
    type: String,
    required: true
  },
  codigo: {
    type: Number,
    required: true
  },
  curso: {
    type: String,
    required: true
  },
  horas: {
    type: Number,
    required: true
  }
})

const Materia = mongoose.model('Materia', materiaSchema)

module.exports = Materia
