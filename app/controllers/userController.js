// const mongoose = require('mongoose')
const User = require('../models/User')
const servicejwt = require('../services/servicejwt')

const login = (req, res) => {
  // const nombre = req.body.nombre
  // const password = req.body.password

  User.findOne({ nombre: req.body.nombre }, (err, usuario) => {
    if (err) {
      return res.status(500).json({
        message: 'Se ha producido un error al obtener la usuario'
      })
    }
    if (!usuario) {
      return res.status(404).json({
        message: 'No tenemos este nombre de usuario'
      })
    } else {
      User.findOne({ password: req.body.password }, (err, usuario) => {
        if (err) {
          return res.status(500).json({
            message: 'Se ha producido un error al obtener la usuario'
          })
        }
        if (!usuario) {
          return res.status(404).json({
            message: 'Contraseña incorrecya'
          })
        }
        // return res.json(usuario)
        return res.status(200).send({ token: servicejwt.createToken(usuario) })
      })
    }
  })
}

module.exports = { login }
