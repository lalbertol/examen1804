const express = require('express') // llamamos a Express
const router = express.Router()
const userController = require('../controllers/userController')
// const auth = require('../middlewares/auth')
// router.use(auth)

router.get('/', (req, res) => {
  userController.index(req, res)
})

router.post('/login', (req, res) => {
  userController.login(req, res)
})

module.exports = router
