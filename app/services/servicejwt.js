const jwt = require('jwt-simple') // módulo Json Web Token
const moment = require('moment') // módulo para fechas
const config = require('../config/config') // definimos una constante "secreta"

function createToken (user) {
  const payload = {
    sub: user._id, // no es muy seguro pero lo simplificamos así
    email: user.email,
    iat: moment().unix(), // fecha creación
    exp: moment()
      .add(230, 'seconds')
      .unix() // valido 1 mes
  }

  return jwt.encode(payload, config.SECRET)
}

function decodeToken (token) {
  return jwt.decode(token, config.SECRET)
}

module.exports = { createToken, decodeToken }
